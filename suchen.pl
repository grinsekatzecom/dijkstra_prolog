:-dynamic pfadVonHinten/2.

dijkstra(Start, Ziel, Pfad, GesamtDist) :-
  knotenBesuchen(Start),                               				/*alle Distanzen suchen*/
  pfadVonHinten([Ziel|PfadVonHinten], Distanz)->                 	/*Ziel gefunden => Ausgabe*/
    reverse([Ziel|PfadVonHinten], Pfad), GesamtDist is Distanz.

pfad(Start,Ziel,Distanz) :- kante(Ziel,Start,Distanz).
pfad(Start,Ziel,Distanz) :- kante(Start,Ziel,Distanz).
 
kuerzererPfad([X|Pfad], Distanz) :-                         		/*Pfad < gespeicherter Pfad => ersetzen*/
  pfadVonHinten([X|_], Dist), !, Distanz < Dist,
  retract(pfadVonHinten([X|_],_)),
  assert(pfadVonHinten([X|Pfad], Distanz)).
kuerzererPfad(Pfad, Distanz) :-                           			/*neuen Pfad speichern*/
  assert(pfadVonHinten(Pfad,Distanz)).
 
knotenBesuchen(Start, Pfad, Distanz) :-                  			/*für alle unbesuchten Nachbarknoten*/
  pfad(Start, Y, Dist), not(member(Y, Pfad)),              			/*Pfad und Distanz aktualisieren*/
  kuerzererPfad([Y,Start|Pfad], Distanz+Dist),               
  knotenBesuchen(Y,[Start|Pfad],Distanz+Dist).              
knotenBesuchen(Start) :-
  retractall(pfadVonHinten(_,_)),knotenBesuchen(Start,[],0).    	/*alles loeschen*/
knotenBesuchen(_).
